export interface User {
  key?: string;
  name: string;
  lastName: string;
  email: string;
  password: string;
  company: string;
  photoUser: string;
  userName: string;
  profile: boolean;
}
