export interface Post {
  key?: string;
  author: string;
  content: string;
  timestamp: any;
  likes: number;
  comments_count: number;
}
