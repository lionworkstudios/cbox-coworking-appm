import {Injectable} from "@angular/core";
import {AngularFireAuth} from "angularfire2/auth";
import {AngularFireDatabase} from "angularfire2/database";
import {User} from "../../models/user/user.model";
import * as firebase from "firebase/app";
import Persistence = firebase.auth.Auth.Persistence;
import {Observable} from "rxjs/Observable";

@Injectable()
export class UserListService {

  private loginS: boolean;
  private registerS: boolean;
  private authUid: string;
  private emailSearch: string;
  private profileCreate: boolean;
  usertList$: Observable<User[]>;
  userTemp = {} as User;

  constructor(private afAuth: AngularFireAuth, private afDatabase: AngularFireDatabase) {
    this.loginS = false;
    this.registerS = false;
  }

  getLoginS() {
    return this.loginS;
  }

  getRegister() {
    return this.registerS;
  }

  getAuthUid() {
    return this.authUid;
  }

  async loginService(user: any) {
      const result = await this.afAuth.auth.signInWithEmailAndPassword(this.emailSearch, user.password);
      if (result && this.afAuth.auth.currentUser.emailVerified && this.profileCreate) {
        this.afAuth.auth.setPersistence(Persistence.LOCAL);
        this.loginS = true;
      } else if (!this.profileCreate) {
        throw new Error("Profile");
    } else {
        throw new Error("Email no verificado");
      }
  }


  async registerService(user: User) {
    try {
      const result = await this.afAuth.auth.createUserWithEmailAndPassword(user.email, user.password);
      if (result) {
        this.registerS = true;
        this.afAuth.authState.take(1).subscribe(auth => {
          user.password = null;
          user.profile = false;
          this.afDatabase.object(`profile/${auth.uid}`).set(user);
          this.authUid = auth.uid;
        });
       await this.afAuth.auth.currentUser.sendEmailVerification();
      }
      console.log(result);
    } catch (e) {
      console.error(e);
    }
  }

  async searchEmailWithUsername(username: string){
    let returnArr: Array<User>[] = [];
    const result = await this.afDatabase.object(`profile/`).query.orderByChild("userName").equalTo(username)
      .on('value', function (snapshot) {
        console.log(snapshot.val());
        snapshot.forEach(function (childSnapshot) {
          let item = childSnapshot.val();
          item.key = childSnapshot.key;
          returnArr = item;
          console.log(returnArr['email']);
          return true;
        });
      });
    if (result) {
      this.emailSearch = returnArr['email'];
      this.profileCreate = returnArr['profile'];
    }
  }

  async updateProfile(user: User) {
    try {
      const result = this.afAuth.authState.take(1).subscribe(auth => {
        user.password = null;
        this.afDatabase.object(`profile/${auth.uid}`).set(user);
      });

      if (result) {
        return true;
      }

    } catch (e) {
      console.error(e);
    }
  }
}
