import {Injectable} from "@angular/core";
import {AngularFireDatabase} from "angularfire2/database";
import {Observable} from "rxjs/Observable";

@Injectable()
export class CompanyListService {

  private companys: Observable<any[]>;

  constructor(private afDatabase: AngularFireDatabase) {
    this.companys = this.afDatabase.list('companys').valueChanges();
  }

  getCompanys() {
    return this.companys;
  }
}
