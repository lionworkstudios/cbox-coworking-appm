import {Injectable} from "@angular/core";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";

@Injectable()
export class ValidatorsService {

  constructor(private formBuilder: FormBuilder) {

  }

  onValidatorsSetProfile() {
    return this.formBuilder.group({
      name: ['', Validators.compose([Validators.required, Validators.pattern('[a-zA-Z]*'), Validators.minLength(8), Validators.maxLength(30)])],
      lastname: ['', Validators.compose([Validators.required, Validators.pattern('[a-zA-Z]*'), Validators.minLength(8), Validators.maxLength(40)])],
      email: ['', Validators.compose([Validators.required, Validators.email])],
      username: ['', Validators.compose([Validators.required, Validators.pattern('[a-zA-Z]*'), Validators.minLength(8), Validators.maxLength(30)])],
      company: ['', Validators.compose([Validators.required])]
    });
  }

  onValidatorsSignUp() {
    return this.formBuilder.group({
      email: ['', Validators.compose([Validators.required, Validators.email])],
      password: ['', Validators.compose([Validators.required, Validators.minLength(6), Validators.maxLength(30), Validators.pattern('^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9]).{6,12}$')])],
      confirmPassword: ['', Validators.required],
    }, {validator: this.matchingPasswords('password','confirmPassword')});
  }

  onValidatorsLogin() {
    return this.formBuilder.group({
      username: ['', Validators.compose([Validators.required, Validators.pattern('[a-zA-Z]*'), Validators.minLength(8), Validators.maxLength(30)])],
      password: ['', Validators.compose([Validators.required])]
    });
  }

  matchingPasswords(passKey: string, passConfirm: string) {
    console.log('entro al match');
    return (group: FormGroup) => {
      let password  = group.controls[passKey];
      let passwordConfirm = group.controls[passConfirm];

      if (password.value !== passwordConfirm.value) {
        return passwordConfirm.setErrors({mismatchedPasswords: true});
      }
    };
  }
}
