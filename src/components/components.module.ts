import { NgModule } from '@angular/core';
import { IonicModule } from 'ionic-angular';
import { PostPopoverComponent } from './post-popover/post-popover';
import { PostAttachmentComponent } from './post-attachment/post-attachment';
import { DemoAvatarComponent } from './demo-avatar/demo-avatar';
import { NetworkStatusComponent } from './network-status/network-status';


@NgModule({
	declarations: [PostPopoverComponent,
    PostAttachmentComponent,
    DemoAvatarComponent,
    NetworkStatusComponent],
	imports: [IonicModule],
	entryComponents: [
		PostPopoverComponent,
		PostAttachmentComponent,
		DemoAvatarComponent,
    NetworkStatusComponent
	],
	exports: [PostPopoverComponent,
    PostAttachmentComponent,
    DemoAvatarComponent,
    NetworkStatusComponent]
})
export class ComponentsModule {}
