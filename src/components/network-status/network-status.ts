import { Component } from '@angular/core';

/**
 * Generated class for the NetworkStatusComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'network-status',
  templateUrl: 'network-status.html'
})
export class NetworkStatusComponent {

  text: string;

  constructor() {
    this.text = 'El contenedor está cerrado.';
  }

}
