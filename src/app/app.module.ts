import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';
import { Keyboard } from '@ionic-native/keyboard';
import { ImagePicker } from '@ionic-native/image-picker';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { IonicStorageModule } from '@ionic/storage';
import { FirebaseConfigApp } from './app.firebase.config';
import { AngularFireModule } from 'angularfire2';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { AngularFireDatabase, AngularFireDatabaseModule } from 'angularfire2/database';
import { AngularFireStorageModule } from 'angularfire2/storage';

import { ComponentsModule } from '../components/components.module';
import { ElasticModule } from 'ng-elastic';
import { DirectivesModule } from '../directives/directives.module';

//import { TabsPage } from '../pages/tabs/tabs';
//import { WelcomePage } from '../pages/welcome/welcome';
import { LoginPage } from '../pages/login/login';
import { SignupPage } from '../pages/signup/signup';
import { MenuPage } from '../pages/menu/menu';
import { HomePage } from '../pages/home/home';
import { PostPage } from '../pages/post/post';
import { NewPostPage } from '../pages/new-post/new-post';
import { NetworkPage } from '../pages/network/network';
import { MessagingPage } from '../pages/messaging/messaging';
import { NotificationsPage } from '../pages/notifications/notifications';
import { JobsPage } from '../pages/jobs/jobs';
import { SearchPage } from '../pages/search/search';
import { ConnectionsPage } from '../pages/connections/connections';
import { MessagePage } from '../pages/message/message';
import { NlbrPipe } from '../pages/message/nlbr.pipe';
import { SettingsPage } from '../pages/settings/settings';
import { ProfilePage } from '../pages/profile/profile';
import { RoomPage } from '../pages/room/room';
import { RoomViewPage } from '../pages/room-view/room-view';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import {PostListService} from "../services/post-list/post-list.service";
import {UserListService} from "../services/user-list/user-list.service";
import {Camera} from "@ionic-native/camera";
import {CompanyListService} from "../services/company/company-list.service";
import {FormsModule} from "@angular/forms";
import {CustomFormsModule} from "ng2-validation";
import {ValidatorsService} from "../services/validators/validators.service";
import {AnimatesDirective, AnimationService} from "css-animator";

export function createTranslateLoader(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

@NgModule({
  declarations: [
    MyApp,
    LoginPage,
    SignupPage,
    MenuPage,
    HomePage,
    PostPage,
    NewPostPage,
    SearchPage,
    NetworkPage,
    MessagingPage,
    NotificationsPage,
    JobsPage,
    ConnectionsPage,
    MessagePage,
    NlbrPipe,
    SettingsPage,
    ProfilePage,
    RoomPage,
    RoomViewPage,
    AnimatesDirective
  ],
  imports: [
    BrowserModule,
    ComponentsModule,
    DirectivesModule,
    ElasticModule,
    HttpClientModule,
    AngularFireModule.initializeApp(FirebaseConfigApp),
    AngularFireAuthModule,
    AngularFireDatabaseModule,
    AngularFireStorageModule,
    IonicModule.forRoot(MyApp, {
      mode: 'ios',// TODO: to have same iOS look for all platforms
      backButtonText: '',
    }),
    IonicStorageModule.forRoot(),
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: (createTranslateLoader),
        deps: [HttpClient]
      }
    }),
    FormsModule,
    CustomFormsModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    LoginPage,
    SignupPage,
    MenuPage,
    HomePage,
    PostPage,
    NewPostPage,
    SearchPage,
    NetworkPage,
    MessagingPage,
    NotificationsPage,
    JobsPage,
    ConnectionsPage,
    MessagePage,
    SettingsPage,
    ProfilePage,
    RoomPage,
    RoomViewPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    Keyboard,
    ImagePicker,
    Camera,
    AngularFireDatabase,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    AnimationService,
    PostListService,
    UserListService,
    CompanyListService,
    ValidatorsService
  ]
})
export class AppModule {}
