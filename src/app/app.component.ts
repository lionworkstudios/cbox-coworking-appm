import { Component, ViewChild } from '@angular/core';
import { Platform, Nav, ToastController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { Keyboard } from '@ionic-native/keyboard';
import { SplashScreen } from '@ionic-native/splash-screen';
import { TranslateService } from '@ngx-translate/core';

import { setTimeout } from 'timers';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage: any = 'WelcomePage';

  @ViewChild(Nav) nav: Nav;
  logged: any;

  public counter = 0;

  transToast: any;

  constructor(private translateService: TranslateService, public toastCtrl: ToastController, public platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen, keyboard: Keyboard) {

    platform.ready().then(() => {

      this.translateService.setDefaultLang('en');
      this.translateService.use('es');

      statusBar.styleDefault();
      splashScreen.hide();
      keyboard.disableScroll(true);
      keyboard.hideKeyboardAccessoryBar(true);

      platform.registerBackButtonAction(() => {
        if (this.counter == 0) {
          console.log("intento de salir")
          this.counter++;
          this.presentToast();
          setTimeout(() => { this.counter = 0}, 3000)
        } else {
          console.log("saliendo")
          platform.exitApp();
        }
      }, 0)
    });
  }

  presentToast(){
    this.translateService.get('APP.EXIT').subscribe(
      value => {
        this.transToast = value;
      }
    );

    let toast = this.toastCtrl.create({
      message: this.transToast,//'Press again to exit',
      duration: 3000,
      position: 'bottom'
    });
    toast.present();
  }
}
