import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the RoomViewPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-room-view',
  templateUrl: 'room-view.html',
})
export class RoomViewPage {

  changeBg: boolean = false;
  titleBar: string;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.titleBar = navParams.get("titleNav");
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RoomViewPage' + this.titleBar);
  }

  doRefresh(refresher) {
    console.log('Begin async operation', refresher);

    setTimeout(() => {
      console.log('Async operation has ended');
      refresher.complete();
    }, 2000);
  }

  segmentChanged(segment: string) {
    switch (segment){
      case 'me':
        this.changeBg = true;
        console.log(`entro al me y el valor es: ${this.changeBg}`);
        break;
      case 'room':
        this.changeBg = false;
        console.log(`entrado a room y el valor es: ${this.changeBg}`);
        break;
      default:
        this.changeBg = false;
        break;
    }
  }

}
