import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { RoomViewPage } from '../room-view/room-view';

/**
 * Generated class for the RoomPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-room',
  templateUrl: 'room.html',
})
export class RoomPage {

  rooms: Array<{name: string, sub_title: string, image: string}>;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.rooms = [
      {name: 'MiniBox', sub_title: 'mini sala de juntas', image: 'minibox5.jpg'},
      {name: 'SuperBox', sub_title: 'Una sala con mayor espacio', image: 'superbox2.jpg' },
      {name: 'CreativeBox', sub_title: 'Has fluir tus ideas', image: 'creative.jpg'}
    ];
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RoomPage');
  }

  goToRoom(titleBar: string){
    this.navCtrl.push(RoomViewPage, {titleNav: titleBar})
      .then(() => {
        console.log("Room View is load");
      })
  }

}
