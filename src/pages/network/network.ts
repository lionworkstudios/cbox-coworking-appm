import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController } from 'ionic-angular';

import { ConnectionsPage } from '../connections/connections';
import { MessagingPage } from '../messaging/messaging';
import { ProfilePage } from '../profile/profile';
import { SearchPage } from '../search/search';
import {MessagePage} from "../message/message";

@IonicPage()
@Component({
  selector: 'page-network',
  templateUrl: 'network.html',
})
export class NetworkPage {

  constructor(public navCtrl: NavController, public navParams: NavParams, public modalCtrl: ModalController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad NetworkPage');
  }

  onViewAllMsm() {
    this.navCtrl.push(MessagingPage, {}).then(() => {
      console.log("Entrando a mensajeria");
    });
  }

  openConnections() {
    let modal = this.modalCtrl.create(ConnectionsPage);
    modal.present();
  }

  goToProfile() {
    this.navCtrl.push(ProfilePage, {})
      .then(() => {
        console.log('Welcome to profile');
      })
  }

  goToSearch() {
    this.navCtrl.push(SearchPage, {})
      .then(() => {
        console.log('Welcome to search');
      })
  }

  goToMessage() {
    this.navCtrl.push(MessagePage, {})
      .then(() => {
        console.log('Welcome to Message');
      })
  }

}
