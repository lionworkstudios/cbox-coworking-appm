import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TranslateModule } from '@ngx-translate/core';
import { NetworkPage } from './network';

@NgModule({
  declarations: [
    NetworkPage,
  ],
  imports: [
    IonicPageModule.forChild(NetworkPage),
    TranslateModule
  ],
})
export class NetworkPageModule {}
