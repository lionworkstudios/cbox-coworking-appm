import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, ModalController, LoadingController, AlertController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { TranslateService } from '@ngx-translate/core';

import { AngularFireAuth } from 'angularfire2/auth';

import { SignupPage } from '../signup/signup';
import { User } from "../../models/user/user.model";
import { UserListService } from "../../services/user-list/user-list.service";
import { FormGroup } from "@angular/forms";
import {ValidatorsService} from "../../services/validators/validators.service";

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  private loginForm: FormGroup;

  loader: any;
  translations: Array<string> = [];
  user = {} as User;

  constructor(
    private userServ: UserListService,
    private translateService: TranslateService,
    private afAuth: AngularFireAuth,
    public validatorServ: ValidatorsService,
    public storage: Storage,
    public aC: AlertController,
    public lC: LoadingController,
    public navCtrl: NavController,
    public navParams: NavParams,
    public viewCtrl: ViewController,
    public modalCtrl: ModalController) {

    this.loginForm = this.validatorServ.onValidatorsLogin();

    this.getTranslations();
    //this.storage.set('authFail',false);
    this.storage.get('authFail').then((r) => {
      if (!r) {
        this.isLoggedIn();
      }
    })
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }

  getTranslations() {
    this.translateService.get('LOGIN.ALERT.TITLE').subscribe(
      value => {
        this.translations.push(value);
      }
    );
    this.translateService.get('LOGIN.ALERT.MSG').subscribe(
      value => {
        this.translations.push(value);
      }
    );
    this.translateService.get('LOGIN.ALERT.BTN').subscribe(
      value => {
        this.translations.push(value);
      }
    );
    this.translateService.get('LOGIN.LOAD').subscribe(
      value => {
        this.translations.push(value);
      }
    );
    console.log(this.translations[2])
  }

  isLoggedIn(){
    this.loader = this.lC.create({
      content: this.translations[3]//'Authenticating...'
    });
    this.loader.present();
    this.afAuth.authState.subscribe(data => {
      if (data) {
        this.loader.dismiss();
        this.storage.set('authFail',false);
        this.goToHome(null,data);
      } else {
        setTimeout(() => {
          this.loader.dismiss();
          this.storage.set('authFail',true);
          let alert = this.aC.create({
            title: this.translations[0],//'Authentication Error',
            subTitle: this.translations[1],//'a session was not found to access',
            cssClass: 'btn-ac-primary',
            buttons: [
              {
                text: 'Ok',
                role: 'cancel'
              },
              {
                text: this.translations[2],//'Retry',
                cssClass: 'button',
                handler: () => {
                  this.isLoggedIn();
                }
              }
            ]
          });
          alert.present();
        }, 3000)
      }
    });
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }

  onSubmit(value: any){
    this.userServ.searchEmailWithUsername(value.username)
      .then(then => {
        this.userServ.loginService(value)
          .then(the => {
            if (this.userServ.getLoginS()) {
              this.storage.set('authFail',false);
              this.navCtrl.setRoot('TabsPage')
                .then(() => {
                  console.log('Welcome to your News Feed!');
                })
            } else {

            }
          })
          .catch( cat => {
            if (cat.message === "Profile") {
              console.log(`Este es el mensaje por no tener perfil ${cat.message}`);
              this.navCtrl.setRoot('SetProfilePage', {
                animation: true, direction: 'forward'
              })
                .then(() => {
                  console.log('Welcome to your News Feed!');
                })
            }
            console.log(`Este es el mesaje de error: ${cat.message}`);
          });
      });
  }

  goToHome(user: User = null, data: any) {
    if (data) {
      this.storage.set('authFail',true);
      this.navCtrl.setRoot('TabsPage')
        .then(() => {
          console.log('Welcome to your News Feed!');
        })
    } else {
      this.userServ.loginService(user);
      if (this.userServ.getLoginS()) {
        this.navCtrl.setRoot('TabsPage')
          .then(() => {
            console.log('Welcome to your News Feed!');
          })
      }
    }
  }

  goToSignup() {
    this.viewCtrl.dismiss();
    let modal = this.modalCtrl.create(SignupPage, {}, {cssClass: 'modal-gradient'});
    modal.present();
  }

}
