import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Camera } from '@ionic-native/camera';
import { AngularFireStorage } from 'angularfire2/storage';
import { UserListService } from "../../services/user-list/user-list.service";
import { User } from "../../models/user/user.model";
import { WelcomePage } from "../welcome/welcome";
import { CompanyListService } from "../../services/company/company-list.service";
import { FormGroup } from '@angular/forms';
import { ValidatorsService } from "../../services/validators/validators.service";
/**
 * Generated class for the SetProfilePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-set-profile',
  templateUrl: 'set-profile.html',
})
export class SetProfilePage {

  private companys: any;
  private setProfileForm: FormGroup;

  public myPhotoRef: any;
  public myPhoto: any;
  public myPhotoURL: any;

  public user = {} as User;

  constructor(public validatorServ: ValidatorsService, private compaServ: CompanyListService, private userServ: UserListService, private storage: AngularFireStorage, private camera: Camera,public navCtrl: NavController, public navParams: NavParams) {
    this.myPhotoURL = 'assets/img/profile/UserDefault.png';
    this.myPhotoRef = this.storage.ref('/Photos/profiles/');
    this.companys = this.compaServ.getCompanys();

    this.setProfileForm = validatorServ.onValidatorsSetProfile();

    // reglas : if request.auth != null
  }

  takePhoto() {
    this.camera.getPicture({
      quality: 100,
      targetWidth: 512,
      targetHeight: 512,
      allowEdit: true,
      correctOrientation : true,
      destinationType: this.camera.DestinationType.DATA_URL,
      sourceType: this.camera.PictureSourceType.CAMERA,
      encodingType: this.camera.EncodingType.PNG,
      saveToPhotoAlbum: true
    }).then(imageData => {
      this.myPhoto = imageData;
      this.uploadPhoto();
    }, error => {
      console.log("ERROR -> " + JSON.stringify(error));
    });
    console.log("Se tomara la foto");
  }

  selectPhoto():void {
    this.camera.getPicture({
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
      destinationType: this.camera.DestinationType.DATA_URL,
      quality: 100,
      targetWidth: 512,
      targetHeight: 512,
      allowEdit: true,
      encodingType: this.camera.EncodingType.PNG,
    }).then(imageData => {
      this.myPhoto = imageData;
      this.uploadPhoto();
    }, error => {
      console.error("Error: -> "+JSON.stringify(error));
    });
    console.log("Se seleccionara la foto");
  }

  onSubmit(value: any):void {
    if (this.setProfileForm.valid) {
      console.log(`EL nombre es: ${value.name}`);
      this.user.photoUser = this.myPhotoURL;
      this.user.name = value.name;
      this.user.lastName = value.lastname;
      this.user.email = value.email;
      this.user.userName = value.username;
      this.user.company = value.company;
      this.user.profile = true;

      this.onSaveProfile();
    }
  }

  onSaveProfile() {
    this.user.photoUser = this.myPhotoURL;
    if (this.userServ.updateProfile(this.user)) {
      this.navCtrl.setRoot('TabsPage');
    }
  }

  onBackPage() {
    this.navCtrl.setRoot(WelcomePage);
  }

  onChange(selectedValue){
    console.log("Selected:",selectedValue);
  }

  private uploadPhoto() {
    this.myPhotoRef.child(this.userServ.getAuthUid()).child('myPhoto.png')
      .putString(this.myPhoto, 'base64', { contentType: 'image/png' })
      .then((savedPicture) => {
        this.myPhotoURL = savedPicture.downloadURL;
      });
  }

  /*private generateUUID(): any {
    var d = new Date().getTime();
    var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx'.replace(/[xy]/g, function (c) {
      var r = (d + Math.random() * 16) % 16 | 0;
      d = Math.floor(d / 16);
      return (c == 'x' ? r : (r & 0x3 | 0x8)).toString(16);
    });
    return uuid;
  }*/

  ionViewDidLoad() {
    console.log('ionViewDidLoad SetProfilePage');
  }

}
