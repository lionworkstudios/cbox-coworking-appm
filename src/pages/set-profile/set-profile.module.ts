import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SetProfilePage } from './set-profile';
import {TranslateModule} from "@ngx-translate/core";

@NgModule({
  declarations: [
    SetProfilePage,
  ],
  imports: [
    IonicPageModule.forChild(SetProfilePage),
    TranslateModule,
  ],
})
export class SetProfilePageModule {}
