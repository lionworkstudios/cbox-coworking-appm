import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, ModalController } from 'ionic-angular';

import { LoginPage } from '../login/login';
import {User} from "../../models/user/user.model";
import {UserListService} from "../../services/user-list/user-list.service";
import {ValidatorsService} from "../../services/validators/validators.service";
import {FormGroup} from "@angular/forms";

@IonicPage()
@Component({
  selector: 'page-signup',
  templateUrl: 'signup.html',
})
export class SignupPage {

  private signUpForm: FormGroup;

  user = {} as User;

  constructor(public validatorServ: ValidatorsService, private userServ: UserListService, public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController, public modalCtrl: ModalController) {
    this.signUpForm = this.validatorServ.onValidatorsSignUp();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SignupPage');
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }

  onSubmit(value: any):void {
    if (this.signUpForm.valid) {
      this.user.email = value.email;
      this.user.password = value.password;

      this.onCreateAcount();
    }
  }

  onCreateAcount(){
    this.userServ.registerService(this.user)
      .then(() => {
        if (this.userServ.getRegister()) {
          this.navCtrl.setRoot('SetProfilePage')
        }
      });
  }

  goToLogin() {
    this.viewCtrl.dismiss();
    let modal = this.modalCtrl.create(LoginPage, {}, {cssClass: 'modal-gradient'});
    modal.present();
  }


}
