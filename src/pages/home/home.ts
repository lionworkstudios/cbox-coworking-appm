import {Component, ViewChild} from '@angular/core';
import { IonicPage, NavController, NavParams, PopoverController, ModalController} from 'ionic-angular';

import { PostPopoverComponent } from '../../components/post-popover/post-popover';
import { PostPage } from '../post/post';
import { NewPostPage } from '../new-post/new-post';
import { SearchPage } from '../search/search';
import { ProfilePage } from '../profile/profile';
import { Post } from "../../models/post/post.model";
import {PostListService} from "../../services/post-list/post-list.service";
import {Observable} from "rxjs/Observable";
import {AnimationBuilder, AnimationService} from "css-animator";

@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
})
export class HomePage {

  @ViewChild('networkstatus') networkStat;
  private animator: AnimationBuilder;

  companySeg: boolean = false;

  postList$: Observable<Post[]>;

  /*posts: Post = {
    author: '',
    content: '',
    timestamp: '',
    likes: undefined,
    comments_count: undefined,
  };*/

  constructor(
    private postServ: PostListService,
    public navCtrl: NavController,
    public navParams: NavParams,
    public popoverCtrl: PopoverController,
    public modalCtrl: ModalController,
    animationServie: AnimationService
    ) {

    this.animator = animationServie.builder();

    this.postList$ = this.postServ
      .getPostList()
      .snapshotChanges()
      .map(changes => {
        return changes.map(c => ({
          key: c.payload.key, ...c.payload.val()
        }));
      });

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad HomePage');
  }

  doNetworkStatus(){
    this.animator.setType('animated infinite bounce').show(this.networkStat.nativeElement);
  }

  doInfinite(infiniteScroll) {
    console.log("Run More Post CBOX");

    setTimeout(() => {
      // post
      console.log("Async operation has ended");
      infiniteScroll.complete();
    }, 500);
  }

  doInfinitePromise(): Promise<any> {
    console.log("Run More Post CBOX in promise");

    return new Promise((resolve) => {
      setTimeout(() => {
        console.log("Async operation has ended");
        resolve();
      },500);
    })
  }

  changeCompanySeg(status: number){
    switch (status){
      case 1:
        this.companySeg = false;
        break;
      case 2:
        this.companySeg = true;
        break;
    }
  }

  presentPopover(event) {
    event.stopPropagation();

    let popover = this.popoverCtrl.create(PostPopoverComponent, {}, {
      cssClass: 'my-popover'
    });
    popover.present({
      ev: event
    });
  }

  goToPost(postId) {
    this.navCtrl.push(PostPage, {
      postId: postId
    })
      .then(() => {
        console.log('Welcome to post:', postId);
      });
  }

  newPost() {
    let modal = this.modalCtrl.create(NewPostPage);
    modal.present();
  }

  goToSearch() {
    this.navCtrl.push(SearchPage, {})
      .then(() => {
        console.log('Welcome to search');
      })
  }

  goToProfile() {
    this.navCtrl.push(ProfilePage, {})
      .then(() => {
        console.log('Welcome to profile');
      })
  }
}
