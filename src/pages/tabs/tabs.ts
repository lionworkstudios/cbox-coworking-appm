import { Component } from '@angular/core';

import { HomePage } from '../home/home';
import { NetworkPage } from '../network/network';
import { RoomPage } from '../room/room';
import { NotificationsPage } from '../notifications/notifications';
import {JobsPage} from '../jobs/jobs';
import {IonicPage} from "ionic-angular";

@IonicPage()
@Component({
  selector: 'tabs-page',
  templateUrl: 'tabs.html',
})
export class TabsPage {

  tab1Root = HomePage;
  tab2Root = NetworkPage;
  tab3Root = RoomPage;
  tab4Root = NotificationsPage;
  tab5Root = JobsPage;

  constructor() {

  }
}
